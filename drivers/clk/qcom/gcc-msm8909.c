// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright 2020 insep
 */

#include <linux/kernel.h>
#include <linux/bitops.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/clk-provider.h>
#include <linux/regmap.h>
#include <linux/reset-controller.h>

#include <dt-bindings/clock/qcom,gcc-msm8909.h>
#include <dt-bindings/reset/qcom,gcc-msm8909.h>

#include "common.h"
#include "clk-regmap.h"
#include "clk-pll.h"
#include "clk-rcg.h"
#include "clk-branch.h"
#include "reset.h"
#include "gdsc.h"

enum {
	P_XO,
	P_GPLL0,
	P_GPLL0_AUX,
	P_BIMC,
	P_GPLL1,
	P_GPLL1_AUX,
	P_GPLL2,
	P_GPLL2_AUX,
	P_SLEEP_CLK,
	P_DSI0_PHYPLL_BYTE,
	P_DSI0_PHYPLL_DSI,
	P_EXT_PRI_I2S,
	P_EXT_SEC_I2S,
	P_EXT_MCLK,
};

static const struct of_device_id gcc_msm8909_match-table[] = {
	{ .compatible = "qcom,gcc-msm8909" },
	{ }
}
MODULE_DEVICE_TABLE(of, gcc_msm8909_match_table);

static int __init gcc_msm8916_init(void)
{
	return platform_driver_register(&gcc_msm8909_driver);
}
core_initcall(gcc_msm8916_init);

static void __exit gcc_msm8909_exit(void)
{
	platform_driver_unregister(&gcc_msm8909_driver);
}
module_exit(gcc_msm8909_exit);

MODULE_DESCRIPTION("Qualcomm GCC MSM8916 Driver");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:gcc-msm8916");
