/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (C) 2015 Imagination Technologies
 * Author: Alex Smith <alex.smith@imgtec.com>
 */
#ifndef __ASM_CLOCKSOURCE_H
#define __ASM_CLOCKSOURCE_H

#define VDSO_ARCH_CLOCKMODES	\
	VDSO_CLOCKMODE_R4K,	\
	VDSO_CLOCKMODE_GIC

#endif /* __ASM_CLOCKSOURCE_H */
